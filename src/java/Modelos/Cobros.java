/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Wilb928
 */
@Entity
@Table(name = "cobros")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cobros.findAll", query = "SELECT c FROM Cobros c"),
    @NamedQuery(name = "Cobros.findByNro", query = "SELECT c FROM Cobros c WHERE c.nro = :nro"),
    @NamedQuery(name = "Cobros.findByCiCliente", query = "SELECT c FROM Cobros c WHERE c.ciCliente = :ciCliente"),
    @NamedQuery(name = "Cobros.findByNombreCliente", query = "SELECT c FROM Cobros c WHERE c.nombreCliente = :nombreCliente"),
    @NamedQuery(name = "Cobros.findByMonto", query = "SELECT c FROM Cobros c WHERE c.monto = :monto"),
    @NamedQuery(name = "Cobros.findByFecha", query = "SELECT c FROM Cobros c WHERE c.fecha = :fecha"),
    @NamedQuery(name = "Cobros.findByCie", query = "SELECT c FROM Cobros c WHERE c.cie = :cie")})
public class Cobros implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Nro")
    private Long nro;
    @Size(max = 10)
    @Column(name = "CI_CLIENTE")
    private String ciCliente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "NOMBRE_CLIENTE")
    private String nombreCliente;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Monto")
    private double monto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Fecha")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fecha;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CIE")
    private String cie;
    @JoinColumn(name = "IdCont", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private Contrato idCont;

    public Cobros() {
    }

    public Cobros(Long nro) {
        this.nro = nro;
    }

    public Cobros(Long nro, String nombreCliente, double monto, Date fecha, String cie) {
        this.nro = nro;
        this.nombreCliente = nombreCliente;
        this.monto = monto;
        this.fecha = fecha;
        this.cie = cie;
    }

    public Long getNro() {
        return nro;
    }

    public void setNro(Long nro) {
        this.nro = nro;
    }

    public String getCiCliente() {
        return ciCliente;
    }

    public void setCiCliente(String ciCliente) {
        this.ciCliente = ciCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public double getMonto() {
        return monto;
    }

    public void setMonto(double monto) {
        this.monto = monto;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getCie() {
        return cie;
    }

    public void setCie(String cie) {
        this.cie = cie;
    }

    public Contrato getIdCont() {
        return idCont;
    }

    public void setIdCont(Contrato idCont) {
        this.idCont = idCont;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nro != null ? nro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cobros)) {
            return false;
        }
        Cobros other = (Cobros) object;
        if ((this.nro == null && other.nro != null) || (this.nro != null && !this.nro.equals(other.nro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return ""+nro;
    }
    
}
