/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Wilb928
 */
@Entity
@Table(name = "empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByCi", query = "SELECT e FROM Empleado e WHERE e.ci = :ci"),
    @NamedQuery(name = "Empleado.findByNombre", query = "SELECT e FROM Empleado e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Empleado.findByAPellidoP", query = "SELECT e FROM Empleado e WHERE e.aPellidoP = :aPellidoP"),
    @NamedQuery(name = "Empleado.findByAPellidoM", query = "SELECT e FROM Empleado e WHERE e.aPellidoM = :aPellidoM"),
    @NamedQuery(name = "Empleado.findByDireccion", query = "SELECT e FROM Empleado e WHERE e.direccion = :direccion"),
    @NamedQuery(name = "Empleado.findByFechaHoraReg", query = "SELECT e FROM Empleado e WHERE e.fechaHoraReg = :fechaHoraReg"),
    @NamedQuery(name = "Empleado.findBySexo", query = "SELECT e FROM Empleado e WHERE e.sexo = :sexo"),
    @NamedQuery(name = "Empleado.findByTelefono", query = "SELECT e FROM Empleado e WHERE e.telefono = :telefono"),
    @NamedQuery(name = "Empleado.findByEmail", query = "SELECT e FROM Empleado e WHERE e.email = :email"),
    @NamedQuery(name = "Empleado.findBySueldo", query = "SELECT e FROM Empleado e WHERE e.sueldo = :sueldo"),
    @NamedQuery(name = "Empleado.findByFechaNac", query = "SELECT e FROM Empleado e WHERE e.fechaNac = :fechaNac")})
public class Empleado implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "CI")
    private String ci;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "Nombre")
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "APellidoP")
    private String aPellidoP;
    @Size(max = 30)
    @Column(name = "APellidoM")
    private String aPellidoM;
    @Size(max = 255)
    @Column(name = "Direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaHoraReg")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHoraReg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Sexo")
    private boolean sexo;
    @Size(max = 10)
    @Column(name = "Telefono")
    private String telefono;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Correo electrónico no válido")//if the field contains email address consider using this annotation to enforce field validation
    @Size(max = 100)
    @Column(name = "Email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Sueldo")
    private double sueldo;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaNac")
    @Temporal(TemporalType.DATE)
    private Date fechaNac;
    @JoinColumn(name = "IdCL", referencedColumnName = "Id")
    @ManyToOne(optional = false)
    private CargoLaboral idCL;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "cie")
    private List<Contrato> contratoList;

    public Empleado() {
    }

    public Empleado(String ci) {
        this.ci = ci;
    }

    public Empleado(String ci, String nombre, String aPellidoP, Date fechaHoraReg, boolean sexo, double sueldo, Date fechaNac) {
        this.ci = ci;
        this.nombre = nombre;
        this.aPellidoP = aPellidoP;
        this.fechaHoraReg = fechaHoraReg;
        this.sexo = sexo;
        this.sueldo = sueldo;
        this.fechaNac = fechaNac;
    }

    public String getCi() {
        return ci;
    }

    public void setCi(String ci) {
        this.ci = ci;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getAPellidoP() {
        return aPellidoP;
    }

    public void setAPellidoP(String aPellidoP) {
        this.aPellidoP = aPellidoP;
    }

    public String getAPellidoM() {
        return aPellidoM;
    }

    public void setAPellidoM(String aPellidoM) {
        this.aPellidoM = aPellidoM;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getFechaHoraReg() {
        return fechaHoraReg;
    }

    public void setFechaHoraReg(Date fechaHoraReg) {
        this.fechaHoraReg = fechaHoraReg;
    }

    public boolean getSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public Date getFechaNac() {
        return fechaNac;
    }

    public void setFechaNac(Date fechaNac) {
        this.fechaNac = fechaNac;
    }

    public CargoLaboral getIdCL() {
        return idCL;
    }

    public void setIdCL(CargoLaboral idCL) {
        this.idCL = idCL;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (ci != null ? ci.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.ci == null && other.ci != null) || (this.ci != null && !this.ci.equals(other.ci))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return nombre+" - "+aPellidoP;
    }
    
}
