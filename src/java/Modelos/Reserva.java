/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelos;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Wilb928
 */
@Entity
@Table(name = "reserva")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Reserva.findAll", query = "SELECT r FROM Reserva r"),
    @NamedQuery(name = "Reserva.findById", query = "SELECT r FROM Reserva r WHERE r.id = :id"),
    @NamedQuery(name = "Reserva.findByFechaEve", query = "SELECT r FROM Reserva r WHERE r.fechaEve = :fechaEve"),
    @NamedQuery(name = "Reserva.findByFechaReg", query = "SELECT r FROM Reserva r WHERE r.fechaReg = :fechaReg"),
    @NamedQuery(name = "Reserva.findByDiasEspera", query = "SELECT r FROM Reserva r WHERE r.diasEspera = :diasEspera"),
    @NamedQuery(name = "Reserva.findByInteresado", query = "SELECT r FROM Reserva r WHERE r.interesado = :interesado"),
    @NamedQuery(name = "Reserva.findByDetalle", query = "SELECT r FROM Reserva r WHERE r.detalle = :detalle")})
public class Reserva implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Short id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaEve")
    @Temporal(TemporalType.DATE)
    private Date fechaEve;
    @Basic(optional = false)
    @NotNull
    @Column(name = "FechaReg")
    @Temporal(TemporalType.DATE)
    private Date fechaReg;
    @Basic(optional = false)
    @NotNull
    @Column(name = "Dias_Espera")
    private short diasEspera;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "INTERESADO")
    private String interesado;
    @Size(max = 255)
    @Column(name = "DETALLE")
    private String detalle;
    @OneToMany(mappedBy = "idRes")
    private List<Contrato> contratoList;

    public Reserva() {
    }

    public Reserva(Short id) {
        this.id = id;
    }

    public Reserva(Short id, Date fechaEve, Date fechaReg, short diasEspera, String interesado) {
        this.id = id;
        this.fechaEve = fechaEve;
        this.fechaReg = fechaReg;
        this.diasEspera = diasEspera;
        this.interesado = interesado;
    }

    public Short getId() {
        return id;
    }

    public void setId(Short id) {
        this.id = id;
    }

    public Date getFechaEve() {
        return fechaEve;
    }

    public void setFechaEve(Date fechaEve) {
        this.fechaEve = fechaEve;
    }

    public Date getFechaReg() {
        return fechaReg;
    }

    public void setFechaReg(Date fechaReg) {
        this.fechaReg = fechaReg;
    }

    public short getDiasEspera() {
        return diasEspera;
    }

    public void setDiasEspera(short diasEspera) {
        this.diasEspera = diasEspera;
    }

    public String getInteresado() {
        return interesado;
    }

    public void setInteresado(String interesado) {
        this.interesado = interesado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    @XmlTransient
    public List<Contrato> getContratoList() {
        return contratoList;
    }

    public void setContratoList(List<Contrato> contratoList) {
        this.contratoList = contratoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reserva)) {
            return false;
        }
        Reserva other = (Reserva) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return id+"-"+interesado;
    }
    
}
